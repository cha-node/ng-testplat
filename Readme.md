# this project was forked by _*[cha/nodetestplat](https://lab.er.co.th/cha/nodetestplat)*_ project

## docker-image dependencies graph

```text
+-- alpine:3.12                                   ====> official image
    |
    +-- node:15.0.1-alpine3.12                    ====> official image
    |   |
    |   +-- anacha/ng:8.0.0-rc.3-alpine           ====> optional ***
    |   `-- anacha/ng:10.2.0-alpine               ====> base images
    |       |
    |      (+)-- anacha/ng-testplat:10.2.0-alpine ====> this project ***
    |       |
    `-------'--- anacha/e2e-testplat:68-alpine    ====> (Optional for other projects)
```

| Image                    | base-0 image    | base-1 image        | base-2 image |
| ------------------------ | --------------- | ------------------- | ------------ |
| ng-testplat:10.2.0-alpine| ng:10.2.0-alpine| node:15.0.1-alpine  | alpine:3.12  |
| ng-testplat:10.1.7-alpine| ng:10.1.7-alpine| node:14.14.0-alpine | alpine:3.12  |
| ng-testplat:10.1.6-alpine| ng:10.1.6-alpine| node:14.13.1-alpine | alpine:3.12  |
| ng-testplat:10.1.2-alpine| ng:10.1.2-alpine| node:14.11.0-alpine | alpine:3.12  |
| ng-testplat:10.1.1-alpine| ng:10.1.1-alpine| node:14.10.1-alpine | alpine:3.12  |
| ng-testplat:10.1.0-alpine| ng:10.1.0-alpine| node:14.9.0-alpine  | alpine:3.12  |
| ng-testplat:10.0.8-alpine| ng:10.0.8-alpine| node:14.9.0-alpine  | alpine:3.12  |
| ng-testplat:10.0.7-alpine| ng:10.0.7-alpine| node:14.8.0-alpine  | alpine:3.12  |
| ng-testplat:10.0.6-alpine| ng:10.0.6-alpine| node:14.8.0-alpine  | alpine:3.12  |
| ng-testplat:10.0.5-alpine| ng:10.0.5-alpine| node:14.7.0-alpine  | alpine:3.12  |
| ng-testplat:10.0.4-alpine| ng:10.0.4-alpine| node:14.6.0-alpine  | alpine:3.12  |
| ng-testplat:10.0.3-alpine| ng:10.0.3-alpine| node:14.5.0-alpine  | alpine:3.12  |
| ng-testplat:10.0.1-alpine| ng:10.0.1-alpine| node:14.5.0-alpine  | alpine:3.12  |
| ng-testplat:10.0.0-alpine| ng:10.0.0-alpine| node:14.4.0-alpine  | alpine:3.11  |
| ng-testplat:9.1.9-alpine | ng:9.1.9-alpine | node:14.4.0-alpine  | alpine:3.11  |
| ng-testplat:9.1.8-alpine | ng:9.1.8-alpine | node:14.4.0-alpine  | alpine:3.11  |
| ng-testplat:9.1.7-alpine | ng:9.1.7-alpine | node:14.3.0-alpine  | alpine:3.11  |
| ng-testplat:9.1.0-alpine | ng:9.1.0-alpine | node:13.12.0-alpine | alpine:3.11  |
| ng-testplat:9.0.7-alpine | ng:9.0.7-alpine | node:13.12.0-alpine | alpine:3.11  |
| ng-testplat:9.0.6-alpine | ng:9.0.6-alpine | node:13.10.1-alpine | alpine:3.11  |
| ng-testplat:9.0.5-alpine | ng:9.0.5-alpine | node:13.10.1-alpine | alpine:3.11  |
| ng-testplat:9.0.4-alpine | ng:9.0.4-alpine | node:13.8.0-alpine  | alpine:3.11  |
| ng-testplat:9.0.3-alpine | ng:9.0.3-alpine | node:13.8.0-alpine  | alpine:3.11  |
| ng-testplat:9.0.2-alpine | ng:9.0.2-alpine | node:13.8.0-alpine  | alpine:3.11  |
| ng-testplat:9.0.1-alpine | ng:9.0.1-alpine | node:13.8.0-alpine  | alpine:3.11  |
| ng-testplat:8.3.25-alpine| ng:8.3.25-alpine| node:13.7.0-alpine  | alpine:3.11  |
| ng-testplat:8.3.24-alpine| ng:8.3.24-alpine| node:13.7.0-alpine  | alpine:3.11  |
| ng-testplat:8.3.23-alpine| ng:8.3.23-alpine| node:13.6.0-alpine  | alpine:3.11  |
| ng-testplat:8.3.22-alpine| ng:8.3.22-alpine| node:13.6.0-alpine  | alpine:3.11  |
| ng-testplat:8.3.21-alpine| ng:8.3.21-alpine| node:13.6.0-alpine  | alpine:3.11  |
| ng-testplat:8.3.20-alpine| ng:8.3.20-alpine| node:13.2.0-alpine  | alpine:3.10  |
| ng-testplat:8.3.19-alpine| ng:8.3.19-alpine| node:13.1.0-alpine  | alpine:3.10  |
| ng-testplat:8.3.18-alpine| ng:8.3.18-alpine| node:13.1.0-alpine  | alpine:3.10  |
| ng-testplat:8.3.17-alpine| ng:8.3.17-alpine| node:13.0.1-alpine  | alpine:3.10  |
| ng-testplat:8.3.16-alpine| ng:8.3.16-alpine| node:13.0.1-alpine  | alpine:3.10  |
| ng-testplat:8.3.15-alpine| ng:8.3.15-alpine| node:13.0.1-alpine  | alpine:3.10  |
| ng-testplat:8.3.14-alpine| ng:8.3.14-alpine| node:13.0.1-alpine  | alpine:3.10  |
| ng-testplat:8.3.12-alpine| ng:8.3.12-alpine| node:12.12.0-alpine | alpine:3.9   |
| ng-testplat:8.3.10-alpine| ng:8.3.10-alpine| node:12.12.0-alpine | alpine:3.9   |
| ng-testplat:8.3.9-alpine | ng:8.3.9-alpine | node:12.12.0-alpine | alpine:3.9   |
| ng-testplat:8.3.8-alpine | ng:8.3.8-alpine | node:12.11.1-alpine | alpine:3.9   |
| ng-testplat:8.3.7-alpine | ng:8.3.7-alpine | node:12.11.1-alpine | alpine:3.9   |
| ng-testplat:8.3.6-alpine | ng:8.3.6-alpine | node:12.10.0-alpine | alpine:3.9   |
| ng-testplat:8.3.5-alpine | ng:8.3.5-alpine | node:12.10.0-alpine | alpine:3.9   |
| ng-testplat:8.3.4-alpine | ng:8.3.4-alpine | node:12.10.0-alpine | alpine:3.9   |
| ng-testplat:8.3.3-alpine | ng:8.3.3-alpine | node:12.10.0-alpine | alpine:3.9   |
| ng-testplat:8.3.2-alpine | ng:8.3.2-alpine | node:12.10.0-alpine | alpine:3.9   |
| ng-testplat:8.3.1-alpine | ng:8.3.1-alpine | node:12.10.0-alpine | alpine:3.9   |
| ng-testplat:8.3.0-alpine | ng:8.3.0-alpine | node:12.10.0-alpine | alpine:3.9   |
| ng-testplat:8.2.2-alpine | ng:8.2.2-alpine | node:12.8.1-alpine  | alpine:3.9   |
| ng-testplat:8.2.1-alpine | ng:8.2.1-alpine | node:12.8.0-alpine  | alpine:3.9   |
| ng-testplat:8.2.0-alpine | ng:8.2.0-alpine | node:12.7.0-alpine  | alpine:3.9   |
| ng-testplat:8.1.3-alpine | ng:8.1.3-alpine | node:12.7.0-alpine  | alpine:3.9   |
| ng-testplat:8.1.2-alpine | ng:8.1.2-alpine | node:12.6.0-alpine  | alpine:3.9   |
| ng-testplat:8.1.0-alpine | ng:8.1.0-alpine | node:12.5.0-alpine  | alpine:3.9   |
| ng-testplat:8.0.6-alpine | ng:8.0.6-alpine | node:12.5.0-alpine  | alpine:3.9   |
| ng-testplat:8.0.5-alpine | ng:8.0.5-alpine | node:12.5.0-alpine  | alpine:3.9   |
| ng-testplat:8.0.4-alpine | ng:8.0.4-alpine | node:12.4.0-alpine  | alpine:3.9   |
| ng-testplat:8.0.3-alpine | ng:8.0.3-alpine | node:12.4.0-alpine  | alpine:3.9   |
| ng-testplat:8.0.2-alpine | ng:8.0.2-alpine | node:12.4.0-alpine  | alpine:3.9   |
| ng-testplat:8.0.1-alpine | ng:8.0.1-alpine | node:12.3.1-alpine  | alpine:3.9   |
| ng-testplat:8.0.0-alpine | ng:8.0.0-alpine | node:12.3.1-alpine  | alpine:3.9   |
| ng-testplat:7.3.9-alpine | ng:7.3.9-alpine | node:12.2.0-alpine  | alpine:3.9   |
| ng-testplat:7.3.8-alpine | ng:7.3.8-alpine | node:11.13.0-alpine | alpine:3.9   |
| ng-testplat:7.3.7-alpine | ng:7.3.7-alpine | node:11.13.0-alpine | alpine:3.9   |
| ng-testplat:7.3.8-alpine | ng:7.3.8-alpine | node:11.13.0-alpine | alpine:3.9   |
| ng-testplat:7.3.6-alpine | ng:7.3.6-alpine | node:11.12.0-alpine | alpine:3.9   |
| ng-testplat:7.3.5-alpine | ng:7.3.5-alpine | node:11.11.0-alpine | alpine:3.9   |
| ng-testplat:7.3.4-alpine | ng:7.3.4-alpine | node:11.10.1-alpine | alpine:3.9   |
| ng-testplat:7.3.3-alpine | ng:7.3.3-alpine | node:11.10.0-alpine | alpine:3.9   |
| ng-testplat:7.3.2-alpine | ng:7.3.2-alpine | node:11.10.0-alpine | alpine:3.9   |
| ng-testplat:7.3.1-alpine | ng:7.3.1-alpine | node:11.9.0-alpine  | alpine:3.8   |
| ng-testplat:7.3.0-alpine | ng:7.3.0-alpine | node:11.9.0-alpine  | alpine:3.8   |
| ng-testplat:7.2.3-alpine | ng:7.2.3-alpine | node:11.7.0-alpine  | alpine:3.8   |
| ng-testplat:7.2.2-alpine | ng:7.2.2-alpine | node:11.7.0-alpine  | alpine:3.8   |
| ng-testplat:7.2.1-alpine | ng:7.2.1-alpine | node:11.6.0-alpine  | alpine:3.8   |
| ng-testplat:7.2.0-alpine | ng:7.2.0-alpine | node:11.6.0-alpine  | alpine:3.8   |
| ng-testplat:6.2.9-alpine | ng:6.2.9-alpine | node:11.6.0-alpine  | alpine:3.8   |
| ng-testplat:7.1.4-alpine | ng:7.1.4-alpine | node:11.5.0-alpine  | alpine:3.8   |
| ng-testplat:7.1.3-alpine | ng:7.1.3-alpine | node:11.4.0-alpine  | alpine:3.8   |
| ng-testplat:7.1.2-alpine | ng:7.1.2-alpine | node:11.3.0-alpine  | alpine:3.8   |
| ng-testplat:7.1.1-alpine | ng:7.1.1-alpine | node:11.3.0-alpine  | alpine:3.8   |
| ng-testplat:7.1.0-alpine | ng:7.1.0-alpine | node:11.3.0-alpine  | alpine:3.8   |
| ng-testplat:6.2.8-alpine | ng:6.2.8-alpine | node:11.2.0-alpine  | alpine:3.8   |
| ng-testplat:7.0.6-alpine | ng:7.0.6-alpine | node:11.1.0-alpine  | alpine:3.8   |
| ng-testplat:6.2.7-alpine | ng:6.2.7-alpine | node:11.1.0-alpine  | alpine:3.8   |
| ng-testplat:7.0.5-alpine | ng:7.0.5-alpine | node:11.1.0-alpine  | alpine:3.8   |
| ng-testplat:7.0.4-alpine | ng:7.0.4-alpine | node:11.0.0-alpine  | alpine:3.8   |
| ng-testplat:7.0.3-alpine | ng:7.0.3-alpine | node:11.0.0-alpine  | alpine:3.8   |
| ng-testplat:7.0.2-alpine | ng:7.0.2-alpine | node:11.0.0-alpine  | alpine:3.8   |
| ng-testplat:6.2.6-alpine | ng:6.2.6-alpine | node:10.12.0-alpine | alpine:3.8   |
| ng-testplat:7.0.1-alpine | ng:7.0.1-alpine | node:10.12.0-alpine | alpine:3.8   |
| ng-testplat:6.2.5-alpine | ng:6.2.5-alpine | node:10.12.0-alpine | alpine:3.8   |
| ng-testplat:6.2.4-alpine | ng:6.2.4-alpine | node:10.11.0-alpine | alpine:3.8   |
| ng-testplat:6.2.3-alpine | ng:6.2.3-alpine | node:10.10.0-alpine | alpine:3.8   |
| ng-testplat:6.2.2-alpine | ng:6.2.2-alpine | node:10.10.0-alpine | alpine:3.8   |
| ng-testplat:6.2.1-alpine | ng:6.2.1-alpine | node:10.10.0-alpine | alpine:3.8   |
| ng-testplat:6.1.5-alpine | ng:6.1.5-alpine | node:10.9.0-alpine  | alpine:3.8   |
| ng-testplat:6.1.4-alpine | ng:6.1.4-alpine | node:10.9.0-alpine  | alpine:3.8   |
| ng-testplat:6.1.3-alpine | ng:6.1.3-alpine | node:10.8.0-alpine  | alpine:3.8   |
| ng-testplat:6.1.2-alpine | ng:6.1.2-alpine | node:10.8.0-alpine  | alpine:3.8   |
| ng-testplat:6.1.1-alpine | ng:6.1.1-alpine | node:10.7.0-alpine  | alpine:3.8   |
| ng-testplat:6.1.0-alpine | ng:6.1.0-alpine | node:10.7.0-alpine  | alpine:3.8   |
| ng-testplat:6.0.8-alpine | ng:6.0.8-alpine | node:10.4.0-alpine  | alpine:3.7   |
| ng-testplat:6.0.7-alpine | ng:6.0.7-alpine | node:10.3.0-alpine  | alpine:3.7   |
| ng-testplat:6.0.5-alpine | ng:6.0.5-alpine | node:10.2.1-alpine  | alpine:3.7   |
| ng-testplat:6.0.3-alpine | ng:6.0.3-alpine | node:10.1.0-alpine  | alpine:3.7   |
| ng-testplat:1.7.4-alpine | ng:1.7.4-alpine | node:9.10.1-alpine  | alpine:3.6   |
| ng-testplat:1.7.3-alpine | ng:1.7.3-alpine | node:9.7.1-alpine   | alpine:3.6   |
| ng-testplat:1.7.2-alpine | ng:1.7.2-alpine | node:9.6.1-alpine   | alpine:3.6   |
| ng-testplat:1.7.1-alpine | ng:1.7.1-alpine | node:9.6.1-alpine   | alpine:3.6   |
| ng-testplat:1.7.0-alpine | ng:1.7.0-alpine | node:9.5.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.8-alpine | ng:1.6.8-alpine | node:9.5.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.7-alpine | ng:1.6.7-alpine | node:9.5.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.6-alpine | ng:1.6.6-alpine | node:9.4.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.5-alpine | ng:1.6.5-alpine | node:9.4.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.4-alpine | ng:1.6.4-alpine | node:9.4.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.3-alpine | ng:1.6.3-alpine | node:9.3.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.2-alpine | ng:1.6.2-alpine | node:9.3.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.1-alpine | ng:1.6.1-alpine | node:9.3.0-alpine   | alpine:3.6   |
| ng-testplat:1.6.0-alpine | ng:1.6.0-alpine | node:9.2.0-alpine   | alpine:3.6   |
| ng-testplat:1.5.5-alpine | ng:1.5.5-alpine | node:9.2.0-alpine   | alpine:3.6   |
| ng-testplat:1.5.4-alpine | ng:1.5.4-alpine | node:9.2.0-alpine   | alpine:3.6   |
| ng-testplat:1.5.3-alpine | ng:1.5.3-alpine | node:9.2.0-alpine   | alpine:3.6   |
| ng-testplat:1.5.2-alpine | ng:1.5.2-alpine | node:9.2.0-alpine   | alpine:3.6   |
| ng-testplat:1.5.2-alpine | ng:1.5.0-alpine | node:9.0.0-alpine   | alpine:3.6   |

## e2e-testplat alpine packages

- ttf-freefont
- eudev=3.2.9-r3
- dbus=1.12.18-r0
- dbus-x11=1.12.18-r0
- xvfb=1.20.9-r1
- chromium=83.0.4103.116-r0
- chromium-chromedriver=83.0.4103.116-r0
- firefox-esr-78.3.0-r0

## Source Repository

- [**cha-node/ng-testplat | Gitlab**](https://gitlab.com/cha-node/ng-testplat) - main repo.
- [anacpe9/ng-testplat | Github](https://github.com/anacpe9/ng-testplat) - mirror.
- [anacpe9/ng-testplat | Bitbucket](https://bitbucket.org/anacpe9/ng-testplat) - mirror.

## License

[MIT](LICENSE)
