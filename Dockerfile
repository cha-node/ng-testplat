FROM anacha/ng:10.2.0-alpine
# FROM node:14.5.0-alpine3.12 AS my-e2e-testplat
LABEL maintainer="Anucha Nualsi <ana.cpe9@gmail.com>"

COPY entrypoint.cha.sh chrome-fix.sh /

ENV DISPLAY=:99 CHROME_BIN=/chrome-fix.sh FIREFOX_BIN=/usr/bin/firefox LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 LANGUAGE=en_US.UTF-8

RUN chmod a+x /entrypoint.cha.sh && \
    chmod a+x /chrome-fix.sh && \
    apk update && \
    apk upgrade && \
    apk add --no-cache --update \
    ttf-freefont \
    "eudev=3.2.9-r3" \
    "dbus=1.12.18-r0" \
    "dbus-x11=1.12.18-r0" \
    "xvfb=1.20.9-r1" \
    "chromium=83.0.4103.116-r0" \
    "chromium-chromedriver=83.0.4103.116-r0" \
    "firefox-esr=78.3.0-r0" && \
    ln -sf /usr/bin/dbus-daemon /bin/dbus-daemon && \
    ln -sf /usr/bin/dbus-launch /bin/dbus-launch && \
    ln -sf /usr/bin/dbus-uuidgen /bin/dbus-uuidgen && \
    ln -sf /usr/bin/dbus-binding-tool /bin/dbus-binding-tool && \
    ln -sf /usr/bin/dbus-cleanup-sockets /bin/dbus-cleanup-sockets && \
    ln -sf /usr/bin/dbus-monitor /bin/dbus-monitor && \
    ln -sf /usr/bin/dbus-run-session /bin/dbus-run-session && \
    ln -sf /usr/bin/dbus-send /bin/dbus-send && \
    ln -sf /usr/bin/dbus-test-tool /bin/dbus-test-tool && \
    ln -sf /usr/bin/dbus-update-activation-environment /bin/dbus-update-activation-environment && \
    sed -i 's/^\ \ \ \ key\ <I380>\ \ {\ \ \ \ \ \ \ \[\ XF86FullScreen\ \ \ \ \ \ \ \ \ ]\ \ \ \ \ \ \ };$/\/\/\ \ \ \ key\ <I380>\ \ {\ \ \ \ \ \ \ \[\ XF86FullScreen\ \ \ \ \ \ \ \ \ ]\ \ \ \ \ \ \ };/g' /usr/share/X11/xkb/symbols/inet && \
    rm -rf /tmp/* && \
    rm -rf /var/tmp/* && \
    rm -rf /var/cache/apk/*

ENTRYPOINT ["/entrypoint.cha.sh"]

# # https://github.com/moby/moby/issues/3378
# FROM node:14.5.0-alpine3.12
# INCLUDE anacha/ng:10.0.3-alpine
# INCLUDE --from=my-e2e-testplat

# LABEL maintainer="Anucha Nualsi <ana.cpe9@gmail.com>"
